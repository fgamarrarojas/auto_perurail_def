@BelmondAndeanExplorer
Feature: 00-[POST] Riesgo Salud (CANAL)
Como usuario Quiero disponer de un flujo que me permita seleccionar una ruta de viaje en tren Para comprar un boleto

	@BelmondAndeanExplorer-ES001-FlujoDeVentaExitoso
	Scenario Outline: ES001_FlujoDeVentaExitoso
	
	Given Me encuentro en la pagina principal de PeruRail
	When Selecciono el destino <destino> y la ruta <ruta> 
	And Selecciono una cabina para un pasajero
	And Introduzco el nombre del pasajero <nombre>, con apellido <apellido>, DNI <DNI>, sexo <sexo>, email <correo>, numero de telefono <nroTlf> y fecha de Nacimiento <fecNac>
	Then Obtengo un pasaje en el tren <tren> por el valor de <valor> dolares
	
	Examples:
	| destino	| ruta											| nombre		| apellido	| DNI					| sexo		| correo														| nroTlf			| fecNac			| tren											|	valor				|
	| "Cusco"	|	"Arequipa > Puno > Cusco"	|"Francisco"| "Gamarra"	| "70226626"	| "Male"	|	"franciscogamarrarojas@gmail.com"	| "902048068"	|	"13-12-1990"|	"Belmond Andean Explorer"	|	"4,025.00"	|
	
	