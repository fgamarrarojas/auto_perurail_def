package bcp.test.def.run;

import net.serenitybdd.cucumber.CucumberWithSerenity;
import org.junit.runner.RunWith;

import io.cucumber.junit.CucumberOptions;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(features = {"src/test/resources/features/"}
		,glue = { "bcp.test.def.definition" })

public class Run {
}
