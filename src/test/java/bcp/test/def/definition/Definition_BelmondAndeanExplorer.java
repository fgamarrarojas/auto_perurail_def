package bcp.test.def.definition;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import net.thucydides.core.annotations.Steps;
import bcp.test.adp.step.RegisterSteps;

public class Definition_BelmondAndeanExplorer {
	
	@Steps
	private RegisterSteps registroStep;
	
	private String ruta = "";
	
	@Given("Me encuentro en la pagina principal de PeruRail")
	public void Me_encuentro_en_la_pagina_principal_de_PeruRail() throws InterruptedException {
		registroStep.ingresoPeruRails();
	}
	
	@When("Selecciono el destino {string} y la ruta {string}")
	public void selecciono_el_destino_y_la_ruta(String destino, String ruta) throws InterruptedException {
		this.ruta = ruta;
		registroStep.seleccionPeruRails(destino,ruta);
	}

	@When("Selecciono una cabina para un pasajero")
	public void selecciono_una_cabina_para_un_pasajero() throws InterruptedException {
		registroStep.cabinaPeruRails(1,1);
	}

	@When("Introduzco el nombre del pasajero {string}, con apellido {string}, DNI {string}, sexo {string}, email {string}, numero de telefono {string} y fecha de Nacimiento {string}")
	public void introduzco_el_nombre_del_pasajero_con_apellido_DNI_sexo_email_numero_de_telefono_y_fecha_de_Nacimiento(String nombre, String apellido, String DNI, String sexo, String email, String nroTlf, String fecNac) throws InterruptedException {
		registroStep.datosPersonalesPeruRails(nombre, apellido, "Peru","Identification Card",DNI,sexo,email,nroTlf,fecNac);
	}

	@Then("Obtengo un pasaje en el tren {string} por el valor de {string} dolares")
	public void obtengo_un_pasaje_en_el_tren_por_el_valor_de_dolares(String tren, String valor) throws InterruptedException {
		registroStep.pagoPeruRails("("+ruta.replace(">", "-")+") 2 Days / 2 Nights :",tren,"Saturday 30 July 2022","1 ADULT","USD "+valor);
	}

}
